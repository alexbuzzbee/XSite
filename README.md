# XSite

XSite is the working name of an XML-based static site generator. XSite transforms a series of XML documents, resources, and data sets into a publishable Web site using its own templating language, XSlots, along with XSLT and optionally other "processors" provided by Python plug-ins.

## Working state

XSite is currently capable of generating simple sites with parameterized hierarchical templates. Resources and other documents can be transcluded into a document, as can the text of per-document parameters and site- or directory-level configuration entries, and templates can themselves be templated. Only XSlots and XSLT are currently available as processors; plug-in processor functionality has yet been implemented. Dataset plug-ins can be used, but the only one that currently comes with XSite is CSV.

Currently, all output documents are named identically to the input documents, and only documents whose names end in `.xml` are processed. This might be changed later, e.g. storing input and/or output XHTML documents under names ending with `.xhtml`.

XSite should not be expected to be bug-free; while it works on the examples in this repository, it might break under some as-yet unknown conditions.

## Format

Input to XSite consists of XML documents, which are usually the main content, resources, which may be XML, non-XML text, or binary files, and tabular datasets.

Each main input document is given as input to a processor (XSlots template, XSLT stylesheet, or plug-in) specified in a processing instruction or as a sitewide default, and the result, usually XHTML, is stored as output under the same name as the input. Resources and datasets can be referenced by the processor.

Resources are grouped into published and unpublished resources; the unpublished resources are only for reference by processors, while the published resources are included in the output.

Datasets consist of a series of records, each consisting of a consistent set of named fields. Datasets can be stored as CSVs, JSON files, or database tables, and are converted into XML for input to other processors.

## XSlots

XSlots is a lightweight templating language intended for "slotting" content into a template document. More complex tasks, such as conditional inclusion, rearranging content within a document, data processing tasks, or output in non-XML formats, or different schemas from the input, should be done using XSLT or plug-in processors.

### Examples

The following is an example of an XSlots template for a site's main layout.

```xml
<?xml version="1.0" encoding="utf-8"?>
<sl:template xmlns:sl="https://www.alm.website/misc/specs/xslots" output="application/xhtml+xml">
  <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <title><sl:content config="site-title" /> - <sl:content param="title" /></title>
      <meta name="keywords">
        <sl:attrib name="content" param="meta-tags" />
      </meta>
      <meta name="keywords">
        <sl:attrib name="content" config="site-meta-tags" />
      </meta>
      <style type="text/css">
        <sl:content resource="unpublished/style.css" />
      </style>
    </head>
    <body>
      <h1><sl:content config="site-title" /></h1>
      <sl:content document="navigation.xml" />
      <h2><sl:content param="title" /></h2>
      <sl:content />
    </body>
  </html>
</sl:template>
```

The following is an example of an XSlots template for a blog archive page, which uses XSLT to generate content from a dataset listing the posts and stored as a CSV file called "posts".

```xml
<?xml version="1.0" encoding="utf-8"?>
<?xsite-template name="main.xml"?>
<?xsite-params title="Archive" meta-tags="blog, blog archive, posts, index"?>
<sl:template xmlns:sl="https://www.alm.website/misc/specs/xslots" xmlns="http://www.w3.org/1999/xhtml" output="application/xhtml+xml">
  <p>The following are all the posts on this blog, most recent first.</p>
  <ul>
    <sl:xslt name="postlist.xsl">
      <sl:content dataset="posts" datatype="csv" />
    </sl:xslt>
  </ul>
</sl:template>
```
