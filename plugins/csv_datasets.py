# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""
CSV dataset module for XSite.
"""
import csv
from lxml import etree

def get_dataset(resources, name):
    """
    Read the specified CSV file from the resources directory and return a corresponding dataset XML tree.
    """
    with open(resources / name) as f:
        reader = csv.DictReader(f)
        dataset = etree.Element("dataset")
        for row in reader:
            record = etree.SubElement(dataset, "record", row)
    return dataset
